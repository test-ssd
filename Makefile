all:
	${CC} -O2 -Wall -o test-ssd-write test-ssd-write.c

.PHONY: clean
clean:
	rm test-ssd-write
